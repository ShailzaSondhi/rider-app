/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 *
 */

import React from 'react';
import { Provider } from 'react-redux';
import { AsyncStorage } from 'react-native';
import { store } from 'shared/store/store';
import NavigationApp from 'shared/navigation';
import SocketIOClient from 'socket.io-client';
import SocketHelper from 'shared/socketIO/index';
import { ApolloProvider } from 'react-apollo';
import client from 'services/apollo';
import config from 'services/config/index';

const configHelper = config();

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  // async componentDidMount() {
  //   this.checkPermission();
  // }

  // // 1
  // async checkPermission() {
  //   const enabled = await firebase.messaging().hasPermission();
  //   if (enabled) {
  //     this.getToken();
  //   } else {
  //     this.requestPermission();
  //   }
  // }

  // // 3
  // async getToken() {
  //   let fcmToken = await AsyncStorage.getItem('fcmToken');
  //   if (!fcmToken) {
  //     fcmToken = await firebase.messaging().getToken();
  //     if (fcmToken) {
  //       // user has a device token
  //       await AsyncStorage.setItem('fcmToken', fcmToken);
  //     }
  //   }
  // }

  // // 2
  // async requestPermission() {
  //   try {
  //     console.log('check permission');
  //     await firebase.messaging().requestPermission();
  //     // User has authorised
  //     this.getToken();
  //   } catch (error) {
  //     // User has rejected permissions
  //     console.log('permission rejected');
  //   }
  // }

  render() {
    // const socket = SocketIOClient(configHelper.apiUrl);
    // socket.emit('connection', 'Hi server');
    // socket.on('connect23', data => {
    //   console.log('Data recieved from server', data);
    // });

    SocketHelper.connect();
    SocketHelper.addListener('connection', 'App', () => {
      SocketHelper.emit('connect23', { data });
    });
    return (
      <ApolloProvider client={client}>
        <Provider store={store}>
          <NavigationApp />
        </Provider>
      </ApolloProvider>
    );
  }
}
