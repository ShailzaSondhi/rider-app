// @flow
import gql from 'graphql-tag';
import client from 'services/apollo';
import { toPromise, catchError } from 'utils';

// Create user
const createUserMutation = gql`
  mutation createUser($name: String!, $email: String, $contact: String!) {
    response: createUser(name: $name, email: $email, contact: $contact) {
      id
      token
      message
      status
    }
  }
`;

export const createUser = (params: TUserData) =>
  toPromise((resolve, reject) => {
    client
      .mutate({
        mutation: createUserMutation,
        variables: params,
      })
      .then(catchError(resolve, reject))
      .catch(reject);
  });

// Update user
const updateUserMutation = gql`
  mutation updateUser($id: Int, $name: String!, $contact: String!) {
    response: updateUser(id: $id, name: $name, contact: $contact) {
      id
      name
      contact
      message
      status
    }
  }
`;

declare type TUserUpdateData = {
  id: Number,
  name: string,
  contact: string,
};

export const updateUser = (params: TUserUpdateData) =>
  toPromise((resolve, reject) => {
    client
      .mutate({
        mutation: updateUserMutation,
        variables: params,
      })
      .then(catchError(resolve, reject))
      .catch(reject);
  });

// Delete user
const deleteUserMutation = gql`
  mutation deleteUser($id: Int) {
    response: deleteUser(id: $id) {
      id
      name
      contact
      message
      status
    }
  }
`;

export const deleteUser = (id: Number) =>
  toPromise((resolve, reject) => {
    client
      .mutate({
        mutation: deleteUserMutation,
        variables: { id },
      })
      .then(catchError(resolve, reject))
      .catch(reject);
  });

// Get all users
const getUsersQuery = gql`
  query getUsers {
    response: getUsers {
      status
      id
      name
      contact
      message
    }
  }
`;

export const getUsers = () =>
  toPromise((resolve, reject) => {
    client
      .query({
        query: getUsersQuery,
        fetchPolicy: 'no-cache',
      })
      .then(catchError(resolve, reject))
      .catch(reject);
  });

// Get one user by id
const getUserByIdQuery = gql`
  query getUserbyId($id: Int) {
    response: getUserbyId(id: $id) {
      status
      id
      name
      contact
      message
    }
  }
`;

export const getUserById = (id: Number) =>
  toPromise((resolve, reject) => {
    client
      .query({
        query: getUserByIdQuery,
        variables: { id },
        fetchPolicy: 'no-cache',
      })
      .then(catchError(resolve, reject))
      .catch(reject);
  });
