// @flow

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Settings from './settings';

const stateSelector = state => ({
  state,
});
const bindActions = dispatch => {
  return bindActionCreators({}, dispatch);
};

export default connect(
  stateSelector,
  bindActions
)(Settings);
