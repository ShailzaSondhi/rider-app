import { HANDLE_SIGNIN } from './actions';

const initialState = {
  name: '',
  contact: '',
  email: '',
};

const userReducer = (state: TUserDataReducer = initialState, action: TAction) => {
  switch (action.type) {
    case HANDLE_SIGNIN:
      return {
        ...state,
        name: action.payload.name,
        contact: action.payload.contact,
        email: action.payload.email,
      };
    default:
      return state;
  }
};

export default userReducer;
