export const HANDLE_SIGNIN = 'HANDLE_SIGNIN';
export const SIGNIN_DONE = 'SIGNIN_DONE';

export const handleSign = data => ({ type: HANDLE_SIGNIN, payload: data });
export const loginDone = data => ({ type: SIGNIN_DONE, payload: data });
