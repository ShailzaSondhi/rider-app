import { StyleSheet, Platform } from 'react-native';
import { Metrics, getHeight, getWidth } from 'utils';
import { Colors, Fonts, FontSize } from 'shared/theme';

const ImageStyle = {
  backgroundImageView: {
    width: Metrics.screenWidth,
    height: getHeight(315),
    opacity: 0.08,
    resizeMode: 'cover',
  },
  scooterImageStyle: {
    position: 'absolute',
    top: getHeight(160),
    width: getWidth(238),
    height: getHeight(200),
    alignSelf: 'center',
  },
  logoImage: {
    width: getWidth(170),
    height: getHeight(50),
    marginTop: getHeight(90),
  },
};

const detailView = {
  mainView: {
    alignItems: 'center',
  },
  passwordView: {
    width: getWidth(278),
    marginTop: 15,
    paddingHorizontal: 15,
    height: 43,

    borderRadius: 6,
    borderColor: 'rgb(174,174,174)',
    borderWidth: 1,
    fontSize: FontSize.base - 1,
    fontFamily: Fonts.TTNormsMedium,
    color: Colors.lightGrey,
  },
};
const phoneInputStyles = {
  phoneInputContainer: {
    width: getWidth(278),
    paddingVertical: 10,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    borderWidth: 1,
    borderRadius: 6,
    borderColor: 'rgb(174,174,174)',
    marginTop: getHeight(60),
  },
  flagView: {
    flexDirection: 'row',
  },
  flagImage: {
    height: 20,
    width: 30,
    borderRadius: 2,
    borderWidth: 0.5,
    borderColor: Colors.lineGrey,
  },
  codeText: {
    marginLeft: 6,
    fontSize: FontSize.base - 1,
    fontFamily: Fonts.TTNormsMedium,
    color: Colors.lightGrey,
  },
  mobileTextInput: {
    height: 18,
    padding: 0,
    width: getWidth(180),
    fontSize: FontSize.base - 1,
    fontFamily: Fonts.TTNormsMedium,
    color: Colors.lightGrey,
    marginLeft: 4,
    marginTop: Platform.OS === 'ios' ? 0 : 2,
  },
};

export default StyleSheet.create({
  container: {
    height: Metrics.screenHeight,
    width: Metrics.screenWidth,
  },
  viewHolder: {
    height: 185,
  },
  safeAreaView: {
    flex: 1,
  },
  detailsTextInput: {
    marginBottom: 5,
  },

  inner: {
    flex: 1,
    alignItems: 'center',
  },
  spinnerStyle: { color: Colors.white },
  phoneInput: {
    backgroundColor: Colors.whisper,
    height: 50,
    marginTop: 20,
    width: Metrics.screenWidth - 50,
    paddingLeft: 20,
    borderRadius: 5,
  },
  signUpPhoneInput: {
    height: 38.3,
    width: 277,
    position: 'absolute',
    top: getHeight(100),
    alignSelf: 'center',
    borderRadius: 6.7,
    borderWidth: 1,
    borderColor: Colors.grey,
    paddingLeft: 15.8,
    zIndex: 1,
  },
  phoneInputPlaceHolder: {
    position: 'absolute',
    top: getHeight(110),
    color: 'rgba(126,126,126,0.4)',
    alignSelf: 'center',
    fontSize: getHeight(17),
    fontFamily: Fonts.TTNormsMedium,
    zIndex: 0,
  },
  logo: {
    width: Metrics.screenWidth - 50,
    marginTop: Metrics.screenHeight * 0.15,
  },
  signUpButton: {
    position: 'absolute',
    top: getHeight(170),
  },

  title: {
    marginTop: 50,
    width: Metrics.screenWidth - 100,
  },
  button: {
    width: getWidth(277),
    height: getHeight(42),
    position: 'absolute',
    top: getHeight(610),
    alignSelf: 'center',
  },
  styleLoginView: {
    width: Metrics.screenWidth,
    height: getHeight(51),
    position: 'absolute',
    top: getHeight(5),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  alreadyAccountText: {
    fontSize: 14,

    color: Colors.tin,
    fontFamily: Fonts.TTNormsMedium,
  },
  loginTextButton: {
    fontSize: 14,
    color: Colors.tin,
    fontFamily: Fonts.TTNormsBold,
  },
  skipTextButtonStyle: {
    position: 'absolute',
    width: Metrics.screenWidth,
    height: getHeight(37),
    top: getHeight(740),
    alignItems: 'center',
    justifyContent: 'center',
  },
  skipTextStyle: {
    fontSize: 14,
    color: Colors.blue,
    fontFamily: Fonts.TTNormsBold,
  },

  //  Image Scroll Styles
  ImageScrollView: {
    width: Metrics.screenWidth,
    height: getHeight(441),
  },

  backgroundImageScrollView: {
    width: Metrics.screenWidth,
    height: getHeight(315),
    opacity: 0.08,
    resizeMode: 'cover',
  },

  headingTextStyle: {
    position: 'absolute',
    top: getHeight(68),
    alignSelf: 'center',

    fontSize: 23.3,
    fontFamily: Fonts.TTNormsBlack,
    color: Colors.greydark,
    letterSpacing: 1.8,
  },
  subHeadingTextStyle: {
    position: 'absolute',
    top: getHeight(100),
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '600',
    letterSpacing: 1.8,
    fontFamily: Fonts.TTNormsRegular,
    color: Colors.greydark,
  },

  mapSubHeadingTextStyle1: {
    position: 'absolute',
    top: getHeight(122),
    alignSelf: 'center',
    fontWeight: '600',
    fontSize: 16,
    letterSpacing: 1.8,
    fontFamily: Fonts.TTNormsRegular,
    color: Colors.greydark,
  },
  mapImageStyle: {
    height: getHeight(200.3),
    width: getWidth(237),
    position: 'absolute',
    top: getHeight(232.3),
    left: getWidth(68),
    resizeMode: 'contain',
  },
  dotsStyle: {
    top: getHeight(387),
    left: getWidth(190),
    width: getWidth(5),
    height: getHeight(5),
  },

  buttonStyle: {
    marginTop: 20,
  },
  textBackground: {
    backgroundColor: Colors.white,
    alignSelf: 'center',
  },
  forgotPasswordText: {
    color: Colors.ultramarineBlue,
    fontFamily: Fonts.TTNormsBold,
    textAlign: 'center',
    lineHeight: getHeight(51.8),
    fontSize: getHeight(15),
  },
  quickLoginText: {
    color: Colors.davyGrey,
    fontFamily: Fonts.TTNormsBold,
    textAlign: 'center',
    lineHeight: getHeight(48),
    fontSize: getHeight(16),
  },
  fingerPrintWrapper: {
    backgroundColor: Colors.cultured,
    height: getHeight(89.3),
    width: getWidth(89.3),
    borderRadius: getHeight(10),
    alignItems: 'center',
    alignSelf: 'center',
    bottom: getHeight(10),
  },
  fingerPrintImage: {
    height: getHeight(46.7),
    width: getWidth(35.3),
    resizeMode: 'stretch',
    top: getHeight(10.3),
    bottom: getHeight(9.7),
  },
  fingerPrintText: {
    lineHeight: getHeight(45.2),
    fontSize: 10.5,
    color: Colors.ultramarineBlue,
    fontFamily: Fonts.TTNormsHeavy,
  },
  ...ImageStyle,
  ...detailView,
  ...phoneInputStyles,
});
