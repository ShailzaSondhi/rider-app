import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SignUp from './sign-up';
import { handleSign } from './redux/actions';

const stateSelector = state => ({
  state,
  loadingStatus: false,
});
const bindActions = dispatch => {
  return bindActionCreators({ handleSign }, dispatch);
};

export default connect(
  stateSelector,
  bindActions
)(SignUp);
