// @flow
import React from 'react';
import {
  View,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  Keyboard,
  Text,
  TextInput,
  SafeAreaView,
  TouchableWithoutFeedback,
} from 'react-native';
import { GeneralButton } from 'shared/components';
import { Scooter, CityBackground, FoodclubLogo, IndianFlag } from 'shared/assets/images';
import { Loader } from 'shared/components/loaders';
import styles from './styles';

type TUserProps = {
  navigation: any,
};

export default class SignUp extends React.Component<TUserProps, null> {
  render() {
    const { loadingStatus, navigation } = this.props;
    const isLoading = loadingStatus;

    return (
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}
      >
        <View style={styles.container}>
          <SafeAreaView style={styles.safeAreaView}>
            {isLoading && <Loader size="large" />}
            <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={3} enabled>
              <ImageBackground source={CityBackground} style={styles.backgroundImageView} />
              <Image source={Scooter} resizeMode="contain" style={styles.scooterImageStyle} />
              <View style={styles.mainView}>
                <Image source={FoodclubLogo} resizeMode="contain" style={styles.logoImage} />

                <View style={styles.phoneInputContainer}>
                  <View style={styles.flagView}>
                    <Image source={IndianFlag} style={styles.flagImage} />

                    <Text style={styles.codeText}>+91</Text>
                    <TextInput
                      textContentType="telephoneNumber"
                      placeholder="Mobile Number"
                      style={styles.mobileTextInput}
                      keyboardType="numeric"
                      // value={number}
                      // onChangeText={text => setNumber(text)}
                    />
                  </View>
                </View>

                <TextInput
                  textContentType="telephoneNumber"
                  placeholder="Password"
                  style={styles.passwordView}
                  secureTextEntry

                  // value={number}
                  // onChangeText={text => setNumber(text)}
                />
              </View>

              <GeneralButton
                text="LOGIN"
                style={styles.buttonStyle}
                onPress={() => navigation.navigate('Order')}
              />
            </KeyboardAvoidingView>
          </SafeAreaView>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  // handleOnPressContinue = (name: string, email: string) => {
  //   const { navigation, handleSign } = this.props;
  //   const contact = navigation.getParam('contact') || '';
  //   const data: TUserData = { name, contact, email };
  //   handleSign(data);
  // };
}
