/* eslint-disable no-console */
import ApolloClient from 'apollo-client';
import { onError } from 'apollo-link-error';
import { setContext } from 'apollo-link-context';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink } from 'apollo-link';
import { store } from 'shared/store/store/index';
import { subscribeOnRequest } from './apolloLinkNetworkStatus';
import buildRequestSubscriber from './requestSubscriber';

const getDefaultHeaders = (operationName: string) => {
  if (operationName === 'createUser') {
    return {
      authorization: '',
    };
  }
  const state = store.getState();
  return {
    authorization: `Bearer ${state.session.accessToken}`,
  };
};

const errorLink = onError(({ operation, graphQLErrors, networkError }) => {
  console.log(`[Apollo Client]: Processing operation: ${operation.operationName}`);

  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) => {
      console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`);
      return { message, locations, path };
    });
  }

  if (networkError) {
    console.log(`[Network error]: ${networkError}`);
  }
});

const bearerLink = setContext((request, previousContext) => {
  const { operationName } = request;
  const { bearerToken, ...restContext } = previousContext;
  const defaultHeaders = getDefaultHeaders(operationName);

  return {
    ...restContext,
    headers: {
      operation: operationName,
      key: 'foodClubIsAmazing',
      ...defaultHeaders,
    },
  };
});

const httpLink = createHttpLink({
  uri: 'http://192.168.1.18:3000/graphql',
  credentials: 'include',
});

const requestSubscriber = subscribeOnRequest(buildRequestSubscriber(store));
console.log(requestSubscriber, 'requestSubscriber');
const client = new ApolloClient({
  // uri: 'http://192.168.1.18:3000/graphql',
  link: ApolloLink.from([errorLink, requestSubscriber, bearerLink, httpLink]),
  // uri: 'http://192.168.1.11:3000/graphql',
  cache: new InMemoryCache(),
  // request: async operationName => {
  //   const { operationName: operation } = operationName;
  //   const defaultHeaders = await getDefaultHeaders(operation);
  //   const headers = {
  //     ...defaultHeaders,
  //     operation,
  //   };
  //   operationName.setContext({ headers });
  // },
});

export default client;
