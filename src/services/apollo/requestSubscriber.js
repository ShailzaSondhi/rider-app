const UNAUTHENTICATED_ERROR_CODE = 401;

const onError = storeProvider => ({ graphQLErrors }) => {
  if (
    graphQLErrors &&
    graphQLErrors.some(({ statusCode }) => statusCode === UNAUTHENTICATED_ERROR_CODE)
  ) {
    storeProvider.store.dispatch();
  }
};

const buildRequestSubscriber = storeProvider => ({
  onRequest: () => {},
  onSuccess: () => {},
  onError: () => onError(storeProvider),
  onCancel: () => {},
});

export default buildRequestSubscriber;
