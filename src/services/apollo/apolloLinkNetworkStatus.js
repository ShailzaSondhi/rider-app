import { ApolloLink, Observable } from 'apollo-link';

export const subscribeOnRequest = () =>
  /**
   * operation:- operation being passed through the link
   * forward:- next link in the chain of the links
   */
  new ApolloLink((operation, forward) => {
    // store.onRequest({ operation });
    const subscriber = forward(operation);
    return new Observable(observer => {
      // let isPending = true;
      const subscription = subscriber.subscribe({
        next: result => {
          // isPending = false;
          // console.log(result, 'resultresultresultresult');
          // if (result.errors) {
          //   store.onError({ operation, graphQLErrors: result.errors });
          // } else {
          //   store.onSuccess({ operation, result });
          // }
          observer.next(result);
        },
        error: networkError => {
          isPending = false;
          // console.log(networkError, 'networkErrornetworkError');
          // store.onError({ operation, networkError });
          observer.error(networkError);
        },
        complete: observer.complete.bind(observer),
      });

      return () => {
        // console.log(isPending, 'isPendingisPendingisPending');
        // if (isPending) store.onCancel({ operation });
        if (subscription) subscription.unsubscribe();
      };
    });
  });

export class ApolloLinkNetworkStatus extends ApolloLink {
  constructor(store) {
    super();
    this.link = subscribeOnRequest(store);
  }

  request(operation, forward) {
    // console.log(forward, 'forwardforward');
    // console.log(operation, 'operationoperation');
    return this.link.request(operation, forward);
  }
}
