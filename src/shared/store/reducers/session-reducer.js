const initialState = {
  accessToken: '',
  expiresIn: 0,
};

const sessionReducer = (state: TSessionDataReducer = initialState, action: TAction) => {
  const accessToken = action.payload ? action.payload.token : '';
  switch (action.type) {
    case 'SIGNIN_DONE':
      return {
        ...state,
        accessToken,
        expiresIn: 0,
      };
    default:
      return state;
  }
};

export default sessionReducer;
