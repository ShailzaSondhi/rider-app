// @flow
import { combineReducers } from 'redux';
// import userReducer from 'screens/user-details/redux/reducer';
// import kitchenReducer from 'screens/order/redux/reducer';
import sessionReducer from './session-reducer';
import { navReducer } from '../../navigation';

export const rootReducer = combineReducers({
  nav: navReducer,
  // user: userReducer,
  // kitchen: kitchenReducer,
  session: sessionReducer,
});
