/* eslint-disable no-console */
// @flow
import { takeLatest, call, put, all } from 'redux-saga/effects';
import { NavigationActions } from 'react-navigation';
// import { REHYDRATE } from 'redux-persist/es/constants';
import { getItem, isEmpty } from 'utils';
import { STARTUP } from '../actions';

function* startUpSaga() {
  try {
    const currentUser = yield call(getItem, 'CURRENT_USER');
    // const currentUser = yield select(state => state.user.name);
    console.log('currentUser', currentUser);

    if (isEmpty(currentUser)) {
      yield put(NavigationActions.navigate({ routeName: 'Auth' }));
    } else {
      yield put(NavigationActions.navigate({ routeName: 'Main' }));
    }
  } catch (exception) {
    console.log(exception, 'exception');
  }
}

export function* watchStartUpSaga(): any {
  // yield all([takeLatest(STARTUP, startUpSaga), takeLatest(REHYDRATE, startUpSaga)]);
  yield all([takeLatest(STARTUP, startUpSaga)]);
}
