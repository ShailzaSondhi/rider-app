// @flow
import { fork } from 'redux-saga/effects';
import { watchAddUser } from 'screens/sign-up/redux/saga';
import { watchStartUpSaga } from './app-saga';

export default function* rootSaga(): any {
  yield fork(watchStartUpSaga);
  yield fork(watchAddUser);
}
