// @flow

import { createStore, applyMiddleware } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
import { logger } from 'redux-logger';
import { rootReducer } from '../reducers/index';
import { navMiddleware } from '../../navigation';
import rootSaga from '../sagas';

const persistConfig = {
  blacklist: ['nav'],
  debug: __DEV__,
  key: 'root',
  keyPrefix: 'v.1',
  storage,
  timeout: 20000,
  whitelist: ['test', 'session'],
};

const sagaMiddleware = createSagaMiddleware();

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
  persistedReducer,
  applyMiddleware(sagaMiddleware, navMiddleware, __DEV__ && logger)
);

persistStore(store, null, () => {
  // eslint-disable-next-line no-console
  console.log({ name: 'Persisted state', value: store.getState() });
});
sagaMiddleware.run(rootSaga);
