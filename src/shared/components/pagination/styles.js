// @flow
// Libraries
import { StyleSheet } from 'react-native';
import { utility } from 'utils';
import { Colors } from 'shared/theme';

// Styles
const styles = StyleSheet.create({
  paginationWrapper: {
    left: 0,
    width: utility.getScreenSize().width,
    height: 20,
    zIndex: 100,
  },
  pagination: {
    flex: 1,
    height: 20,
    flexDirection: 'row',
  },
  paginationDot: {
    width: 10,
    height: 10,
    margin: 5,
    backgroundColor: Colors.white,
    borderRadius: 5,
    opacity: 0.3,
  },
  Opacity: {
    opacity: 1,
  },
  centerContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
