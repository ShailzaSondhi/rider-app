// @flow
import React from 'react';
import { View, FlatList } from 'react-native';
import styles from './styles';

type TPaginationProps = {
  size: number,
  style: any,
  Index: number,
};

export function Pagination(props: TPaginationProps) {
  const array = [];
  const { size, style, Index } = props;
  for (let i = 0; i < size; i += 1) array.push('a');

  return (
    <View style={[styles.paginationWrapper, style]}>
      <FlatList
        style={styles.pagination}
        scrollEnabled={false}
        contentContainerStyle={[styles.pagination, styles.centerContent]}
        horizontal
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        data={array}
        renderItem={({ item, index }) => {
          if (Index === index && item)
            return <View style={[styles.paginationDot, styles.Opacity]} />;
          return <View style={styles.paginationDot} />;
        }}
      />
    </View>
  );
}
