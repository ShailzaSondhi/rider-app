// @flow
import React, { useState } from 'react';
import { Left, Right, Body } from 'native-base';
import { StyleSheet, Text, View } from 'react-native';
import { LinkButton } from '../buttons';
import { Colors, Fonts, FontSize } from '../../theme';

type TBaseCounterProps = {
  style?: any,
  textStyle?: any,
  textStyle2?: any,
  countStyle?: any,
  plusStyle?: any,
  minusStyle?: any,
  counterValue: number,
  onPlusPress: (val: number) => void,
  onMinusPress: (val: number) => void,
};

export const BaseCounter = (props: TBaseCounterProps) => {
  const {
    style,
    textStyle,
    countStyle,
    textStyle2,
    plusStyle,
    minusStyle,
    counterValue,
    onPlusPress,
    onMinusPress,
  } = props;
  const [count, setCount] = useState(counterValue);

  function handlePlus() {
    setCount(count + 1);
    onPlusPress(count + 1);
  }

  function handleMinus() {
    setCount(count - 1);
    onMinusPress(count - 1);
  }

  return (
    <View style={[styles.container, style]}>
      <Left>
        <LinkButton
          text="-"
          style={[styles.plus, plusStyle]}
          textStyle={[styles.text, textStyle]}
          onPress={handleMinus}
        />
      </Left>
      <Body>
        <Text style={countStyle}>{count}</Text>
      </Body>
      <Right>
        <LinkButton
          text="+"
          style={[styles.minus, minusStyle]}
          textStyle={[styles.text2, textStyle2]}
          onPress={handlePlus}
        />
      </Right>
    </View>
  );
};

BaseCounter.defaultProps = {
  style: null,
  textStyle: null,
  textStyle2: null,
  countStyle: null,
  plusStyle: null,
  minusStyle: null,
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 19.2,
    height: 38.3,
    width: 150,
    flexDirection: 'row',
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: Colors.grey,
  },
  plus: {
    width: 50,
  },
  minus: {
    width: 50,
  },
  text: {
    fontSize: FontSize.base,
    fontFamily: Fonts.TTNormsMedium,
    color: Colors.grey,
  },
  text2: {
    fontSize: FontSize.base,
    fontFamily: Fonts.TTNormsMedium,
    color: Colors.ultramarineBlue,
  },
});
