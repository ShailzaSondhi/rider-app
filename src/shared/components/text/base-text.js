// @flow
import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { Fonts, FontSize } from '../../theme';

/*
 * Base text component that should be used
 * instead of the base react native component
 */

type TBaseTextProps = Text.propTypes;

export const BaseText = (props: TBaseTextProps) => {
  const { style, children } = props;
  return (
    <Text style={[styles.txt, style]} {...props}>
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  txt: {
    fontFamily: Fonts.TTNormsRegular,
    fontSize: FontSize.base,
    textAlign: 'center',
  },
});
