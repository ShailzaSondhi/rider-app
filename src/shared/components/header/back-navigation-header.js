import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Body, Left, Right } from 'native-base';
import { utility, Metrics, getHeight, getWidth } from 'utils';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { IconButton } from '../buttons/icon-button';
import { Colors } from '../../theme';

type THeaderProps = {
  container: any,
  body: TElements,
  right: TElements,
  bodyStyle: any,
  rightStyle: any,
  navigation: TNavigation,
  size?: number,
  iconStyle?: any,
};

export const BackNavigationHeader = (props: THeaderProps) => {
  const { container, body, right, bodyStyle, rightStyle, navigation, size, iconStyle } = props;
  return (
    <View style={[styles.container, container]}>
      <Left>
        <IconButton
          style={styles.leftButton}
          icon={
            <MaterialIcon
              name="keyboard-arrow-left"
              size={size}
              color={Colors.ultramarineBlue}
              style={iconStyle}
            />
          }
          onPress={() => {
            navigation.goback();
          }}
        />
      </Left>
      {body && <Body style={bodyStyle}>{body}</Body>}
      {right && <Right style={rightStyle}>{right}</Right>}
    </View>
  );
};

BackNavigationHeader.defaultProps = {
  size: 35,
  iconStyle: null,
};

const statusbarHeight = utility.getStatusBarHeight();

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: statusbarHeight,
    minHeight: Metrics.navBarHeight,
  },
  leftButton: {
    height: getHeight(50),
    width: getHeight(50),
    marginLeft: getWidth(19),
    backgroundColor: Colors.transparent,
  },
});
