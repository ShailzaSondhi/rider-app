// @flow
import * as React from 'react';
import { View, Image, Text, ImageBackground } from 'react-native';
import Swiper from 'react-native-swiper';
import { Scooter, CityBackground, MapImage } from 'shared/assets/images';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { IconButton } from 'shared/components';
import { Colors } from 'shared/theme';
import styles from './styles';

type TAuthSwiperProps = {
  title: string,
  subtitle: string,
  subtitle2: string,
  navigation: any,
  showIcon?: boolean,
};

const arr = [
  {
    title: 'QUICK AND SAFE',
    subTitle: 'DELIVERY',
    subTitle2: '',
    imageSource: Scooter,
    style: styles.ScooterImageStyle,
  },
  {
    title: 'ORDER FROM BEST',
    subTitle: 'RESTAURANT AROUND',
    subTitle2: 'YOU',
    imageSource: MapImage,
    style: styles.mapImageStyle,
  },
];

export class AuthSwiper extends React.PureComponent<TAuthSwiperProps, any> {
  static defaultProps = {
    showIcon: true,
  };

  render() {
    const { title, subtitle, subtitle2, navigation, showIcon } = this.props;
    return (
      <View style={styles.ImageScrollView}>
        <ImageBackground source={CityBackground} style={styles.backgroundImageScrollView} />
        <View style={styles.innerView}>
          {showIcon && (
            <IconButton
              style={styles.leftButton}
              icon={<MaterialIcon name="keyboard-arrow-left" size={30} color={Colors.blueTint} />}
              onPress={() => {
                navigation.goback();
              }}
            />
          )}
          <Text style={styles.headingTextStyle}>{title}</Text>
          <Text style={styles.subHeadingTextStyle}>{subtitle}</Text>
          {!subtitle2 && <Text style={styles.mapSubHeadingTextStyle1}>{subtitle2}</Text>}
          <Swiper paginationStyle={styles.dotsStyle}>
            {arr.map((item, index) => {
              return this.renderItem(item, index);
            })}
          </Swiper>
        </View>
      </View>
    );
  }

  renderItem = (item: any, index: number) => (
    <View key={index}>
      <Image style={item.style} source={item.imageSource} />
    </View>
  );
}
