// @flow
// Library
import { utility, Metrics, getHeight, getWidth } from 'utils';
import { Colors, Fonts, FontSize } from 'shared/theme';

// Constants
const { width, height } = utility.getScreenSize();

// Styling
const styles = {
  slider: {
    height: width * 0.75,
  },
  sliderCard: {
    top: 0,
    width,
    height: width * 0.75,
    flex: 1,
    justifyContent: 'flex-end',
  },
  sliderImage: {
    width,
    height: width * 0.75,
    top: 0,
    position: 'absolute',
  },
  Opacity: {
    opacity: 0.6,
  },
  pagination: {
    marginTop: height * -0.04,
    width,
  },

  ImageScrollView: {
    width: Metrics.screenWidth,
    height: getHeight(441),
  },
  innerView: { position: 'absolute', top: 0 },
  backgroundImageScrollView: {
    width: Metrics.screenWidth,
    height: getHeight(315),
    opacity: 0.08,
    resizeMode: 'cover',
  },
  ScooterImageStyle: {
    height: getHeight(200.3),
    width: getWidth(237),
    position: 'absolute',
    top: getHeight(188.3),
    left: getWidth(68),
    resizeMode: 'contain',
  },
  headingTextStyle: {
    position: 'absolute',
    top: getHeight(64.3),
    alignSelf: 'center',
    fontSize: FontSize.xLarge,
    fontFamily: Fonts.TTNormsBold,
    color: Colors.grey,
    letterSpacing: 1.8,
  },
  subHeadingTextStyle: {
    position: 'absolute',
    top: getHeight(91.7),
    alignSelf: 'center',
    fontSize: FontSize.base,
    letterSpacing: 1.8,
    fontFamily: Fonts.TTNormsRegular,
    color: Colors.grey,
  },

  mapSubHeadingTextStyle1: {
    position: 'absolute',
    top: getHeight(110.7),
    alignSelf: 'center',
    fontSize: FontSize.base,
    letterSpacing: 1.8,
    fontFamily: Fonts.TTNormsRegular,
    color: Colors.grey,
  },
  mapImageStyle: {
    height: getHeight(200.3),
    width: getWidth(237),
    position: 'absolute',
    top: getHeight(232.3),
    left: getWidth(68),
    resizeMode: 'contain',
  },
  dotsStyle: {
    top: getHeight(387),
    left: getWidth(190),
    width: getWidth(5),
    height: getHeight(5),
  },
  leftButton: {
    height: 26.7,
    width: getWidth(45.3),
    marginLeft: getWidth(14.7),
    top: getHeight(30),
  },
};

export default styles;
