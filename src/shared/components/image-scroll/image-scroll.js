// @flow
import React from 'react';
import { View, FlatList, Image } from 'react-native';
import FastImage from 'react-native-fast-image';
import { Pagination } from 'shared/components/pagination';
import { GradBlackRev } from 'shared/assets/images';
import styles from './styles';

type TImagesProps = {
  item: any,
  index: number,
  onScroll: (event: any) => void,
};

export const ImageScroll = (props: TImagesProps) => {
  const { item, index, onScroll } = props;

  return (
    <View>
      <FlatList
        removeClippedSubviews
        scrollEventThrottle={32}
        onScroll={onScroll}
        contentContainerStyle={styles.slider}
        pagingEnabled
        horizontal
        keyExtractor={(Item, Index) => Index.toString()}
        showsHorizontalScrollIndicator={false}
        data={item.images}
        renderItem={Item => {
          return (
            <View style={styles.sliderCard}>
              <FastImage
                style={styles.sliderImage}
                source={{ uri: Item.item, priority: FastImage.priority.normal }}
              />
              <Image
                resizeMode="stretch"
                resizeMethod="resize"
                style={[styles.sliderImage, styles.Opacity]}
                source={GradBlackRev}
              />
            </View>
          );
        }}
      />
      <Pagination Index={index} style={styles.pagination} size={item.images.length} />
    </View>
  );
};
