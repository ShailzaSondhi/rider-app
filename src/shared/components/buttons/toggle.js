/* eslint-disable react-native/no-inline-styles */
// @flow
import React, { useState } from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import { getWidth } from 'utils';
import { Colors, Fonts, FontSize } from '../../theme';

type TToggleProps = {
  style: any,
  textStyle: any,
  text: string,
  activeColor: string,
  inActiveColor: string,
};

export const Toggle = (props: TToggleProps) => {
  const [checked, setChecked] = useState(false);
  const { style, textStyle, text, activeColor, inActiveColor, ...otherProps } = props;
  const active = activeColor || Colors.ultramarineBlue;
  const inActive = inActiveColor || Colors.white;
  return (
    <TouchableOpacity
      style={[styles.button, style]}
      onPress={() => {
        setChecked(!checked);
      }}
      {...otherProps}
    >
      <View style={[styles.toggleView, { backgroundColor: checked ? active : inActive }]} />
      <Text style={[styles.buttonText, textStyle]}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  button: {
    alignItems: 'center',
    width: getWidth(81),
    flexDirection: 'row',
  },
  buttonText: {
    color: Colors.black,
    textAlign: 'center',
    fontSize: FontSize.base,
    fontFamily: Fonts.TTNormsMedium,
    marginLeft: 5,
  },
  toggleView: {
    height: 13,
    width: 13,
    borderWidth: 1,
    borderColor: Colors.lightGrey,
  },
};
