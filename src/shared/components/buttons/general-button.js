// @flow
import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors, Fonts, FontSize } from 'shared/theme';
import { getHeight, getWidth } from 'utils';

type TGeneralButtonProps = {
  style: any,
  buttonHolderStyle: any,
  textStyle: any,
  text: string,
};

export const GeneralButton = (props: TGeneralButtonProps) => {
  const { style, text, buttonHolderStyle, textStyle, ...otherProps } = props;
  return (
    <TouchableOpacity style={[styles.buttonWrapper, style]} {...otherProps}>
      <LinearGradient
        colors={[Colors.ultramarineBlue, Colors.pictonBlue]}
        start={{ x: 0, y: 0 }}
        end={{ x: 1.8, y: 0 }}
        style={[styles.buttonHolder, buttonHolderStyle]}
      >
        <Text style={[styles.buttonText, textStyle]}>{text}</Text>
      </LinearGradient>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonWrapper: {
    height: getHeight(46.63),
    width: getWidth(277),
    alignSelf: 'center',
    shadowColor: Colors.blueShadow,
    shadowOffset: {
      height: getHeight(1.2),
      width: getWidth(2),
    },
    shadowRadius: 0.8,
    shadowOpacity: getHeight(10),
  },
  buttonHolder: {
    borderRadius: getWidth(19.2),
  },
  buttonText: {
    color: Colors.white,
    textAlign: 'center',
    lineHeight: getHeight(48.7),
    letterSpacing: 0.867,
    fontSize: FontSize.mediumSmall,
    fontFamily: Fonts.TTNormsBold,
  },
});
