// @flow
import React from 'react';
import { StyleSheet } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import { Colors } from '../../theme';

type TBaseDropDownProps = {
  data: any,
  selectedItem?: (item: any) => {},
  containerStyle: any,
  pickerStyle: any,
  baseColor: String,
  value: String,
};

export const BaseDropDown = (props: TBaseDropDownProps) => {
  const { data, value, containerStyle, baseColor, pickerStyle, selectedItem } = props;
  // const [value, setValue] = useState(data[0].value);
  return (
    <Dropdown
      value={value}
      data={data}
      itemColor={Colors.black}
      selectedItemColor={Colors.black}
      baseColor={baseColor || Colors.red}
      fontSize={13}
      textColor={Colors.black}
      lineWidth={0}
      // eslint-disable-next-line no-shadow
      onChangeText={(value, index, data) => {
        selectedItem && selectedItem(data[index].value);
      }}
      containerStyle={[styles.containerStyle, containerStyle]}
      pickerStyle={[styles.pickerStyle, pickerStyle]}
      dropdownPosition={0}
    />
  );
};

BaseDropDown.defaultProps = {
  selectedItem: () => {},
};

const styles = StyleSheet.create({
  containerStyle: {
    height: 38,
    justifyContent: 'center',
    paddingBottom: 15,
    paddingHorizontal: 15,
  },
  pickerStyle: {
    marginTop: 15,
  },
});
