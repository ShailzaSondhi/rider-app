// @flow
import { Fonts } from 'shared/theme';
import { getHeight, getWidth } from 'utils';

const styles = {
  bottomTabsContainer: {
    alignItems: 'center',
  },
  bottomTabsImage: {
    height: getHeight(22.7),
    width: getWidth(17.3),
  },
  bottomTabsText: {
    fontSize: 11.5,
    fontFamily: Fonts.TTNormsBold,
  },
};

export default styles;
