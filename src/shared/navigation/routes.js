// @flow
import SignUp from 'screens/sign-up';
import { BottomTabs } from './bottom-tabs';

export const authRoutes = {
  SignUp,
};
export const mainRoutes = {
  Tab: BottomTabs,
};
