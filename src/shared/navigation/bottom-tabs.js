// @flow
import React from 'react';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { createBottomTabNavigator } from 'react-navigation';
import { Platform, Image, Text, View } from 'react-native';
import { BagIcon, SearchIcon, SettingsIcon, DiningIcon } from 'shared/assets/images';
import Settings from 'screens/settings';
import { Colors } from 'shared/theme';
import { Metrics } from 'utils';
import styles from './styles';

const tabOptions = (tabScreen, iconName, iconTitle) => {
  return {
    screen: tabScreen,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <View style={styles.bottomTabsContainer}>
          <Image source={iconName} style={{ ...styles.bottomTabsImage, tintColor }} />
          <Text style={{ ...styles.bottomTabsText, color: tintColor }}>{iconTitle}</Text>
        </View>
      ),
    },
  };
};

const tabRoutesConfig = {
  Order: tabOptions(Settings, BagIcon, 'Order'),
  Business: tabOptions(Settings, DiningIcon, 'Dine Out'),
  Redeem: tabOptions(Settings, SearchIcon, 'Search'),
  Settings: tabOptions(Settings, SettingsIcon, 'Settings'),
  Menu: tabOptions(Settings, DiningIcon, 'Menu'),
};

let bottomTabs = createMaterialBottomTabNavigator(tabRoutesConfig, {
  labeled: false,
  activeTintColor: Colors.blueTint,
  inactiveTintColor: Colors.gray,
  barStyle: { backgroundColor: Colors.black },
});
if (Platform.OS === 'ios') {
  bottomTabs = createBottomTabNavigator(tabRoutesConfig, {
    tabBarOptions: {
      style: {
        width: Metrics.screenWidth,
        height: Metrics.tabBarHeight,
      },
      showIcon: true,
      showLabel: false,
      activeTintColor: Colors.blueTint,
      inactiveTintColor: Colors.gray,
    },
  });
}

export const BottomTabs = bottomTabs;
