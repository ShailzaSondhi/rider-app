// @flow
import SocketIOClient from 'socket.io-client';
import config from 'services/config';

const configHelper = config();

let socket;
let listeners = {};
let tempListeners = {};

const addListener = (event: string, listenerId: string, listener: TCallback) => {
  let primaryListenerArr = listeners;
  let isEventExisted = false;
  if (!socket) {
    isEventExisted = true;
    primaryListenerArr = tempListeners;
  }
  let listenerArr = {};
  if (primaryListenerArr[event]) {
    if (socket) {
      isEventExisted = true;
    }
    listenerArr = primaryListenerArr[event];
  }
  listenerArr[listenerId] = listener;
  if (socket) {
    listeners[event] = listenerArr;
  } else {
    tempListeners[event] = listenerArr;
  }

  if (!isEventExisted) {
    if (socket) {
      socket.on(event, data => {
        // eslint-disable-next-line no-console
        console.log('evenrt', data);
        // const listenersToSendData = listeners[event];
        // if (listenersToSendData) {
        //   let isContainChatScreenListener = false;
        //   const keys = Object.keys(listenersToSendData);
        //   for (let index = 0; index < keys.length; index += 1) {
        //     const tmpKey = keys[index];
        //     if (tmpKey === 'chatscreen' && event === 'message') {
        //       isContainChatScreenListener = true;
        //       break;
        //     }
        //   }
        //   keys.forEach(key => {
        //     if (key === 'chatscreen' || !isContainChatScreenListener) {
        //       const listenerObj = listenersToSendData[key];
        //       listenerObj(data);
        //     }
        //   });
        // }
      });
    }
  }
};

const connect = () => {
  if (!socket) {
    socket = SocketIOClient(configHelper.apiUrl);
    if (tempListeners) {
      Object.keys(tempListeners).forEach(key => {
        const eventListeners = tempListeners[key];
        Object.keys(eventListeners).forEach(listenerVal => {
          addListener(key, listenerVal, eventListeners[listenerVal]);
        });
      });
    }
  }
};

const disconnect = () => {
  if (socket) {
    socket.close();
    socket = undefined;
    listeners = {};
    tempListeners = {};
  }
};

const emit = (event: string, data: any) => {
  if (socket) {
    socket.emit(event, data);
  }
};

const removeListener = (event: string, listenerId: string) => {
  if (listeners[event]) {
    const listenerForEvents = listeners[event];
    if (listenerForEvents) {
      delete listenerForEvents[listenerId];
    }
  }
};

export default {
  connect,
  disconnect,
  emit,
  addListener,
  removeListener,
};
