// @flow
declare type TUserData = {
  name: string,
  contact: string,
  email: string,
};

declare type TUserDataReducer = TUserData;
