// @flow
declare type TSessionData = {
  accessToken: string,
  expiresIn: number,
};

declare type TSessionDataReducer = TSessionData;
