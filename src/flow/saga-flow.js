// @flow
declare type TSaga = Generator<*, *, *>;

declare type TAction = {
  type: string,
  payload: TPayload,
};

declare type TPayload = {
  [string]: any,
};
